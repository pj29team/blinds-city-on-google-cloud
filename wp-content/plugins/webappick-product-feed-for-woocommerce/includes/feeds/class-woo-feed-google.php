<?php

/**
 * Class Google
 *
 * Responsible for processing and generating feed for Google.com
 *
 * @since 1.0.0
 * @package Google
 *
 */
class Woo_Feed_Google
{

    /**
     * This variable is responsible for holding all product attributes and their values
     *
     * @since   1.0.0
     * @var     array $products Contains all the product attributes to generate feed
     * @access  public
     */
    public $products;

    /**
     * This variable is responsible for holding feed configuration form values
     *
     * @since   1.0.0
     * @var     array $rules Contains feed configuration form values
     * @access  public
     */
    public $rules;

    /**
     * This variable is responsible for mapping store attributes to merchant attribute
     *
     * @since   1.0.0
     * @var     array $mapping Map store attributes to merchant attribute
     * @access  public
     */
    public $mapping;

    /**
     * This variable is responsible for generate error logs
     *
     * @since   1.0.0
     * @var     array $errorLog Generate error logs
     * @access  public
     */
    public $errorLog;

    /**
     * This variable is responsible for making error number
     *
     * @since   1.0.0
     * @var     int $errorCounter Generate error number
     * @access  public
     */
    public $errorCounter;

    /**
     * Feed Wrapper text for enclosing each product information
     *
     * @since   1.0.0
     * @var     string $feedWrapper Feed Wrapper text
     * @access  public
     */
    public $feedWrapper = 'item';

    /**
     * Store product information
     *
     * @since   1.0.0
     * @var     array $storeProducts
     * @access  public
     */
    private $storeProducts;

    /**
     * Define the core functionality to generate feed.
     *
     * Set the feed rules. Map products according to the rules and Check required attributes
     * and their values according to merchant specification.
     * @var Woo_Generate_Feed $feedRule Contain Feed Configuration
     * @since    1.0.0
     */
    public function __construct($feedRule)
    {
        $products = new Woo_Feed_Products();
        $storeProducts = $products->woo_feed_get_visible_product();
        $engine = new WF_Engine($storeProducts, $feedRule);
        $this->products = $engine->mapProductsByRules();
        $this->rules = $feedRule;
        if ($feedRule['feedType'] == 'xml') {
            $this->identifier_status_add();
            $this->mapAttributeForXML();
            $this->process_google_shipping_attribute_for_xml();
        } else {
            $this->mapAttributeForCSVTXT();
            $this->process_google_shipping_attribute_for_CSVTXT();
        }
    }


    /**
     * Return Feed
     *
     * @return array|bool|string
     */
    public function returnFinalProduct()
    {
        $engine = new WF_Engine($this->products, $this->rules);
        if ($this->rules['feedType'] == 'xml') {
            return $this->get_feed($this->products);
        } elseif ($this->rules['feedType'] == 'txt') {
            return $engine->get_txt_feed();
        } elseif ($this->rules['feedType'] == 'csv') {
            return $engine->get_csv_feed();
        }
        return false;
    }

    /**
     * Configure merchant attributes for XML feed
     */
    public function mapAttributeForXML()
    {
        //Basic product information
        $this->mapAttribute('id', 'g:id', true);
        $this->mapAttribute('title', 'title', true);
        $this->mapAttribute('description', 'description', true);
        $this->mapAttribute('link', 'link', true);
        $this->mapAttribute('product_type', 'g:product_type', true);
        $this->mapAttribute('current_category', 'g:google_product_category', true);
        $this->mapAttribute('image', 'g:image_link', true);
        $this->mapAttribute('images', 'g:additional_image_link', false);
        $this->mapAttribute('condition', 'g:condition', false);

        //Availability & Price
        $this->mapAttribute('availability', 'g:availability', false);
        $this->mapAttribute('price', 'g:price', false);
        $this->mapAttribute('sale_price', 'g:sale_price', false);
        $this->mapAttribute('sale_price_effective_date', 'g:sale_price_effective_date', true);

        //Unique Product Identifiers
        $this->mapAttribute('brand', 'g:brand', true);
        $this->mapAttribute('sku', 'g:mpn', true);
        $this->mapAttribute('upc', 'g:gtin', true);
        $this->mapAttribute('identifier_exists', 'g:identifier_exists', true);

        //Detailed Product Attributes
        $this->mapAttribute('item_group_id', 'g:item_group_id', false);
        $this->mapAttribute('color', 'g:color', true);
        $this->mapAttribute('gender', 'g:gender', true);
        $this->mapAttribute('age_group', 'g:age_group', false);
        $this->mapAttribute('material', 'g:material', true);
        $this->mapAttribute('pattern', 'g:pattern', true);
        $this->mapAttribute('size', 'g:size', true);
        $this->mapAttribute('size_type', 'g:size_type', true);
        $this->mapAttribute('size_system', 'g:size_system', true);

        //Tax & Shipping
        //$this->mapAttribute('tax', 'g:tax', false);
        $this->mapAttribute('weight', 'g:shipping_weight', false);
        $this->mapAttribute('length', 'g:shipping_length', false);
        $this->mapAttribute('width', 'g:shipping_width', false);
        $this->mapAttribute('height', 'g:shipping_height', false);
        $this->mapAttribute('shipping_label', 'g:shipping_label', false);
        $this->mapAttribute('shipping_country', 'g:shipping_country', false);
        $this->mapAttribute('shipping_region', 'g:shipping_region', false);
        $this->mapAttribute('shipping_service', 'g:shipping_service', false);
        $this->mapAttribute('shipping_price', 'g:shipping_price', false);


        //Product Combinations
        $this->mapAttribute('multipack', 'g:multipack', true);
        $this->mapAttribute('is_bundle', 'g:is_bundle', true);
        $this->mapAttribute('adult', 'g:adult', true);

        //Adults Product
        $this->mapAttribute('adult', 'g:adult', true);

        //AdWord Attributes
        $this->mapAttribute('adwords_redirect', 'g:adwords_redirect', true);

        //Custom Label Attributes for Shopping Campaigns
        $this->mapAttribute('custom_label_0', 'g:custom_label_0', true);
        $this->mapAttribute('custom_label_1', 'g:custom_label_1', true);
        $this->mapAttribute('custom_label_2', 'g:custom_label_2', true);
        $this->mapAttribute('custom_label_3', 'g:custom_label_3', true);
        $this->mapAttribute('custom_label_4', 'g:custom_label_4', true);

        //Additional Attributes
        $this->mapAttribute('excluded_destination', 'g:excluded_destination', true);
        $this->mapAttribute('expiration_date', 'g:expiration_date', true);

        //Unit Prices
        $this->mapAttribute('unit_pricing_measure', 'g:unit_pricing_measure', true);
        $this->mapAttribute('unit_pricing_base_measure', 'g:unit_pricing_base_measure', true);

        //Energy Labels
        $this->mapAttribute('energy_efficiency_class', 'g:energy_efficiency_class', true);

        //Loyalty Points (Japan Only)
        $this->mapAttribute('loyalty_points', 'g:loyalty_points', true);

        //Multiple Installments (Brazil Only)
        $this->mapAttribute('installment', 'g:installment', true);

        //Merchant Promotions Attribute
        $this->mapAttribute('promotion_id', 'g:promotion_id', true);
    }

    /**
     * Configure merchant attributes for XML feed
     */
    public function mapAttributeForCSVTXT()
    {
        //Basic product information
        $this->mapAttribute('id', 'id', false);
        $this->mapAttribute('title', 'title', true);
        $this->mapAttribute('description', 'description', true);
        $this->mapAttribute('link', 'link', true);
        $this->mapAttribute('mobile_link', 'mobile link', true);
        $this->mapAttribute('product_type', 'product type', true);
        $this->mapAttribute('current_category', 'google product category', true);
        $this->mapAttribute('image', 'image link', true);
        $this->mapAttribute('images_1', 'additional image link 1', true);
        $this->mapAttribute('images_2', 'additional image link 2', true);
        $this->mapAttribute('images_3', 'additional image link 3', true);
        $this->mapAttribute('images_4', 'additional image link 4', true);
        $this->mapAttribute('images_5', 'additional image link 5', true);
        $this->mapAttribute('images_6', 'additional image link 6', true);
        $this->mapAttribute('images_7', 'additional image link 7', true);
        $this->mapAttribute('images_8', 'additional image link 8', true);
        $this->mapAttribute('images_9', 'additional image link 9', true);
        $this->mapAttribute('images_10', 'additional image link 10', true);
        $this->mapAttribute('condition', 'condition', false);

        //Availability & Price
        $this->mapAttribute('availability', 'availability', false);
        $this->mapAttribute('price', 'price', false);
        $this->mapAttribute('sale_price', 'sale price', false);
        $this->mapAttribute('sale_price_effective_date', 'sale price effective date', true);

        //Unique Product Identifiers
        $this->mapAttribute('brand', 'brand', true);
        $this->mapAttribute('sku', 'mpn', true);
        $this->mapAttribute('upc', 'gtin', true);
        $this->mapAttribute('identifier_exists', 'identifier exists', true);

        //Detailed Product Attributes
        $this->mapAttribute('item_group_id', 'item group id', false);
        $this->mapAttribute('color', 'color', true);
        $this->mapAttribute('gender', 'gender', true);
        $this->mapAttribute('age_group', 'age group', false);
        $this->mapAttribute('material', 'material', true);
        $this->mapAttribute('pattern', 'pattern', true);
        $this->mapAttribute('size', 'size', true);
        $this->mapAttribute('size_type', 'size type', true);
        $this->mapAttribute('size_system', 'size system', true);

        //Tax & Shipping
        $this->mapAttribute('tax', 'tax', false);
        $this->mapAttribute('weight', 'shipping weight', false);
        $this->mapAttribute('length', 'shipping length', false);
        $this->mapAttribute('width', 'shipping width', false);
        $this->mapAttribute('height', 'shipping height', false);
        $this->mapAttribute('shipping_label', 'shipping label', false);
        $this->mapAttribute('shipping_country', 'shipping country', false);
        $this->mapAttribute('shipping_region', 'shipping region', false);
        $this->mapAttribute('shipping_service', 'shipping service', false);
        $this->mapAttribute('shipping_price', 'shipping price', false);

        //Product Combinations
        $this->mapAttribute('multipack', 'multipack', true);
        $this->mapAttribute('is_bundle', 'is bundle', true);
        $this->mapAttribute('adult', 'adult', true);

        //Adults Product
        $this->mapAttribute('adult', 'adult', true);

        //AdWord Attributes
        $this->mapAttribute('adwords_redirect', 'adwords redirect', true);

        //Custom Label Attributes for Shopping Campaigns
        $this->mapAttribute('custom_label_0', 'custom label 0', true);
        $this->mapAttribute('custom_label_1', 'custom label 1', true);
        $this->mapAttribute('custom_label_2', 'custom label 2', true);
        $this->mapAttribute('custom_label_3', 'custom label 3', true);
        $this->mapAttribute('custom_label_4', 'custom label 4', true);

        //Additional Attributes
        $this->mapAttribute('excluded_destination', 'excluded destination', true);
        $this->mapAttribute('expiration_date', 'expiration date', true);

        //Unit Prices
        $this->mapAttribute('unit_pricing_measure', 'unit pricing measure', true);
        $this->mapAttribute('unit_pricing_base_measure', 'unit pricing base measure', true);

        //Energy Labels
        $this->mapAttribute('energy_efficiency_class', 'energy efficiency class', true);

        //Loyalty Points (Japan Only)
        $this->mapAttribute('loyalty_points', 'loyalty points', true);

        //Multiple Installments (Brazil Only)
        $this->mapAttribute('installment', 'installment', true);

        //Merchant Promotions Attribute
        $this->mapAttribute('promotion_id', 'promotion id', true);
    }

    /**
     * Map to google attribute
     * @param $from
     * @param $to
     * @param bool $cdata
     */
    public function mapAttribute($from, $to, $cdata = false)
    {
        $i = 0;
        if ($this->products) {
            foreach ($this->products as $no => $product) {
                foreach ($product as $key => $value) {
                    if ($key == $from) {
                        unset($this->products[$no][$from]);
                        if ($this->rules['feedType'] == 'xml') {
                            $this->products[$no][$to] = $this->formatXMLLine($to, $value, $cdata);
                        } else {
                            $this->products[$no][$to] = $value;
                        }
                    }
                }
                $i++;
            }
        }
    }

    public function identifier_status_add()
    {
        $identifier = array('brand', 'upc', 'sku', 'EAN', 'JAN', 'ISBN', 'ITF-14', 'mpn', 'gtin');
        if ($this->products) {
            foreach ($this->products as $no => $product) {
                if ($this->rules['feedType'] == 'xml') {
                    if (count(array_intersect_key(array_flip($identifier), $product)) >=2) {
                        # Any 2 required keys exist!
                        $this->products[$no]["g:identifier_exists"] = $this->formatXMLLine("g:identifier_exists", "TRUE", $cdata = true);
                    } else {
                        $this->products[$no]["g:identifier_exists"] = $this->formatXMLLine("g:identifier_exists", "FALSE", $cdata = true);
                    }
                }
            }
        }
    }

    public function process_google_shipping_attribute_for_xml()
    {
        $shipping = array('g:shipping_country', 'g:shipping_service', 'g:shipping_price','g:shipping_region');
        $shippingAttr = array();
        $i = 0;
        if ($this->products) {
            foreach ($this->products as $no => $products) {
                foreach ($products as $keyAttr => $valueAttr) {
                    if (in_array($keyAttr, $shipping)) {
                        array_push($shippingAttr, array($keyAttr => $valueAttr));
                        unset($this->products[$no][$keyAttr]);
                    }
                }
                if (count($shippingAttr)) {
                    $str = "";
                    foreach ($shippingAttr as $key => $attributes) {
                        foreach ($attributes as $keyAttr => $valueAttr) {
                            $str .= str_replace("shipping_", "", $valueAttr);
                        }
                    }
                    $this->products[$no]['g:shipping'] = $this->formatXMLLine("g:shipping", $str, false);
                }
                $i++;
                $shippingAttr = array();
            }
        }
    }

    public function process_google_shipping_attribute_for_CSVTXT()
    {
        $shipping = array('shipping country', 'shipping service', 'shipping price','shipping region');
        $shippingAttr = array();
        $i = 0;
        if ($this->products) {
            foreach ($this->products as $no => $products) {
                foreach ($products as $keyAttr => $valueAttr) {
                    if (in_array($keyAttr, $shipping)) {
                        array_push($shippingAttr, array($keyAttr => $valueAttr));
                        unset($this->products[$no][$keyAttr]);
                    }
                }
                if (count($shippingAttr)) {
                    $str = "";
                    foreach ($shippingAttr as $key => $attributes) {
                        foreach ($attributes as $keyAttr => $valueAttr) {
                            $country = ($keyAttr == "shipping country") ? $str .= $valueAttr . ":" : "";
                            $region = ($keyAttr == "shipping region") ? $str .= $valueAttr . ":" : "";
                            $service = ($keyAttr == "shipping service") ? $str .= $valueAttr . ":" : "";
                            $price = ($keyAttr == "shipping price") ? $str .= $valueAttr : "";
                        }
                    }
                    $this->products[$no]['shipping(country:service:price)'] = str_replace(" : ", ":", $str);
                }
                $i++;
                $shippingAttr = array();
            }
        }
    }

    function formatXMLLine($attribute, $value, $cdata, $space ="")
    {
        //Make single XML  node
        if(!empty($value))
            $value=trim($value);
        if (gettype($value) == 'array')
            $value = json_encode($value);
        if (strpos($value, "<![CDATA[") === false && substr(trim($value), 0, 4) == "http") {
            $value = "<![CDATA[$value]]>";
        } elseif (strpos($value, "<![CDATA[") === false && $cdata === true && !empty($value)) {
            $value = "<![CDATA[$value]]>";
        }

        return "
        $space<$attribute>$value</$attribute>";
    }


    public function get_feed_header()
    {
        $output = '<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0" xmlns:g="http://base.google.com/ns/1.0" xmlns:c="http://base.google.com/cns/1.0">
  <channel>
    <title>' . get_option('blogname') . '</title>
    <link><![CDATA['.site_url().']]></link>
    <description>'.get_option('blogdescription').'</description>';
        return $output;
    }

    public function get_feed($items)
    {
        $feed = "";
        $feed .= $this->get_feed_header();
        $feed .= "\n";
        if ($items) {
            foreach ($items as $item => $products) {
                $feed .= "      <" . $this->feedWrapper . ">";
                foreach ($products as $key => $value) {
                    if (substr($key, 0, 7) == 'images_' && !empty($value)) {
                        $key = "g:additional_image_link";
                        $feed .= $this->formatXMLLine($key, $value, true);

                    } else {
                        if (!empty($value))
                            $feed .= $value;
                    }
                }
                $feed .= "\n      </" . $this->feedWrapper . ">\n";
            }
            $feed .= $this->get_feed_footer();

            return $feed;
        }
        return false;
    }

    public function get_feed_footer()
    {
        $footer = "  </channel>
</rss>";
        return $footer;
    }

    public function short_products()
    {
        if ($this->products) {
            update_option('wpf_progress', "Shorting Products");
            sleep(1);
            $array = array();
            $ij = 0;
            foreach ($this->products as $key => $item) {
                $array[$ij] = $item;
                unset($this->products[$key]);
                $ij++;
            }
            return $this->products = $array;
        }
        return $this->products;
    }

    /**
     * Responsible to make CSV feed
     * @return string
     */
    public function get_csv_feed()
    {
        if ($this->products) {
            $headers = array_keys($this->products[0]);
            $feed[] = $headers;
            foreach ($this->products as $no => $product) {
                $row = array();
                foreach ($headers as $key => $header) {
                    if (strpos($header, "additional image link") !== false) {
                        $header = "additional image link";
                    }
                    $row[] = isset($product[$header]) ? $product[$header] : "";;
                }
                $feed[] = $row;
            }
            return $feed;
        }
        return false;
    }

}