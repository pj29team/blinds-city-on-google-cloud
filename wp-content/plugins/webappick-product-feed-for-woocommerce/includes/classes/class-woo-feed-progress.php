<?php

/**
 * Created by PhpStorm.
 * User: wahid
 * Date: 2/13/2016
 * Time: 6:52 PM
 */
header('Content-Type: text/event-stream');
// recommended to prevent caching of event data.
header('Cache-Control: no-cache');

class Woo_Feed_Progress
{
    public function send_message($message, $progress)
    {
        sleep(1);
        $id = array('message' => $message, 'progress' => $progress);

        echo "id: $id" . PHP_EOL;
        echo "data: " . json_encode($id) . PHP_EOL;
        echo PHP_EOL;

        ob_flush();
        flush();
    }
}

$progress = new Woo_Feed_Progress();
for ($i = 1; $i <= 10; $i++) {
    $progress->send_message($i, 'on iteration ' . $i . ' of 10', $i * 10);

    sleep(1);
}