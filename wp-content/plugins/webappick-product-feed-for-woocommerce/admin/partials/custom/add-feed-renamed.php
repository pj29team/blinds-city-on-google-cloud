<div class="container">
    <section>
        <div class="tabs tabs-style-bar">
            <nav>
                <ul>
                    <li class="tab-current"><a href="#section-bar-1" class="icon icon-tools"><span>Content</span></a>
                    </li>
                    <li class=""><a href="#section-bar-2" class="icon icon-upload"><span>Filter</span></a></li>
                    <li class=""><a href="#section-bar-3" class="icon icon-upload"><span>FTP</span></a></li>
                </ul>
            </nav>
            <div class="content-wrap">
                <section id="section-bar-1" class="content-current">
                    <table class="table tree widefat fixed sorted_table mtable" width="100%" id="table-1">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Merchant Attributes</th>
                            <th>Prefix</th>
                            <th>Type</th>
                            <th>Value</th>
                            <th>Suffix</th>
                            <th>Output Type</th>
                            <th>Output Limit</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <i class="wf_sortedtable dashicons dashicons-menu"></i>
                            </td>
                            <td>
                                <input type="text" name="mattributes[]" autocomplete="off" required class="wf_mattributes"/>
                            </td>
                            <td>
                                <input type="text" name="prefix[]" autocomplete="off" class="wf_ps"/>
                            </td>
                            <td>
                                <select name="type[]" class="attr_type">
                                    <option value="attribute">Attribute</option>
                                    <option value="pattern">Pattern</option>
                                </select>
                            </td>
                            <td>
                                <select name="attributes[]" id="" class="wf_attr wf_attributes">
                                    <?php echo $product->attributeDropdown(); ?>
                                </select>
                                <input type="text" name="default[]" autocomplete="off" class="wf_default wf_attributes"
                                       style=" display: none;"/>
                            </td>
                            <td>
                                <input type="text" name="suffix[]" autocomplete="off" class="wf_ps"/>
                            </td>
                            <td>
                                <select name="output_type[][]" id="" class="outputType" >
                                    <option value="1">Default</option>
                                    <option value="2">Strip Tags</option>
                                    <option value="3">UTF-8 Encode</option>
                                    <option value="4">htmlentities</option>
                                    <option value="5">Integer</option>
                                    <option value="6">Price</option>
                                    <option value="7">Remove Space</option>
                                    <option value="8">CDATA</option>
                                </select>
                                <i class="dashicons dashicons-editor-expand expandType"></i>
                                <i style="display: none;" class="dashicons dashicons-editor-contract contractType"></i>
                            </td>
                            <td>
                                <input type="text" name="limit[]" class="wf_ps"/>
                            </td>
                            <td>
                                <i class="delRow dashicons dashicons-trash"></i>
                            </td>
                        </tr>

                        </tbody>
                        <tfoot>
                        <tr>
                            <td>
                                <button type="button" class="button-small button-primary" id="wf_newRow">Add New
                                    Row
                                </button>
                            </td>
                            <td colspan="8">

                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </section>
                <section id="section-bar-2" class="">
                    <table>

                    </table>
                </section>
                <section id="section-bar-3" class="">
                    <table class="table widefat fixed mtable" width="100%" >
                        <tbody>
                        <tr>
                            <td>Enabled</td>
                            <td>
                                <select name="enabled" id="">
                                    <option value="0">Disabled</option>
                                    <option value="1">Enabled</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Protocol</td>
                            <td>
                                <select name="enabled" id="">
                                    <option value="ftp">FTP/FTPS</option>
                                    <option value="sftp">SFTP</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Host Name</td>
                            <td><input type="text" name="ftphost"/></td>
                        </tr>
                        <tr>
                            <td>User Name</td>
                            <td><input type="text" name="ftpuser"/></td>
                        </tr>
                        <tr>
                            <td>Password</td>
                            <td><input type="password" name="ftppassword"/></td>
                        </tr>
                        <tr>
                            <td>Path</td>
                            <td><input type="text" name="ftppath"/></td>
                        </tr>
                        <tr>
                            <td>Passive Mode</td>
                            <td>
                                <select name="passive" id="">
                                    <option value="0">Disabled</option>
                                    <option value="1">Enabled</option>
                                </select>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </section>
            </div>
            <!-- /content -->
        </div>
    </section>
</div>
<!-- /container -->
<table class=" widefat fixed">
    <tr>
        <td align="right">
            <button type="submit" id="wf_submit" class="wfbtn">
                Save & Generate Feed
            </button>
        </td>
    </tr>
</table>
