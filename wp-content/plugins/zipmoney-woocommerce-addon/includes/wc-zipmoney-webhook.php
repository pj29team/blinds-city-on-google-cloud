<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

/**
 * Class for handling webhook request
 *
 * @class    WC_ZipMoney_WebHook
 * @version  1.1.0
 * @package  zipMoney Payments
 * @author   zipMoney Payments
 */
class WC_ZipMoney_WebHook extends ZipMoney_Abstract_WebHook
{

  /**
   *
   * @param  string $merchantId, string $merchantKey,boolen $log_enabled (optional)
   */
  public function __construct($merchantId,$merchantKey,$log_enabled = false)
  {
      parent::__construct($merchantId,$merchantKey);

      $this->log_enabled = $log_enabled;
  }

  /** 
   * Sets the tnx_id to the order.
   *
   * @access public
   * @param  int $order_id, string $merchtxn_id
   */
  public function setTxnId($order_id,$txn_id)
  {
    update_post_meta( $order_id, '_zipmoney_txn_id', $txn_id );
  }
 
  /** 
   * Returns the tnx_id to the order.
   *
   * @access public
   * @param  int $order_id,
   * @return string $txn_id
   */
  public function getTxnId($order_id)
  {
    return get_post_meta( $order_id, '_zipmoney_txn_id', true );
  }

	/**
   * Process Authorisation Success
   *
   * @access protected
   * @throws ZipMoney_Exception
   * @param  $response
   */
  protected function _eventAuthSuccess($response)
  {
    WC_ZipMoney::staticLog("WebHook Data:- ".json_encode($response), $this->log_enabled);

    try{
    	if($response->status=='Authorised'){
    		$order = new WC_Order($response->order_id);
            
        if(!isset($order->id))
            throw new ZipMoney_Exception("Order with the given id not found");
        if(
          $order->get_status() == "cancelled"  ||
          $order->get_status() == "refunded"    
        )
          throw new ZipMoney_Exception("The order has been refunded or cancelled");

        $order->update_status('wc-zip-authorised', 'ZipMoney');

        $this->setTxnId($response->order_id,$response->txn_id );

        WC_ZipMoney::staticLog(sprintf("ZipMoney Authorised for [Order Id %s ]",$response->order_id), $this->log_enabled);
    	}
    } catch (Exception $e){
      WC_ZipMoney::staticLog($e->getMessage(),$this->log_enabled);
    }
  	die();
  }

  /**
   * Process Authorisation Failure
   *
   * @access protected   
   * @throws ZipMoney_Exception
   * @param  $response
   */
  protected function _eventAuthFail($response)
  {      
    WC_ZipMoney::staticLog("WebHook Data:- ".json_encode($response), $this->log_enabled);
    try{   
      $order = new WC_Order($response->order_id);
            
      if(!isset($order->id))
        throw new ZipMoney_Exception("Order with the given id not found");
       
       $order -> update_status('wc-failed', 'ZipMoney');
    	 $order -> add_order_note(sprintf('ZipMoney Authorise Failed %s ',$response->error_message));

    	  WC_ZipMoney::staticLog(sprintf("ZipMoney Authorised Failed [Order Id %s]. ",$response->order_id), $this->log_enabled);
    } catch (Exception $e){
      WC_ZipMoney::staticLog($e->getMessage(),$this->log_enabled);
    }
		die();
  }

  /**
   * Process Authorisation Review
   *   
   * @access protected   
   * @throws ZipMoney_Exception
   * @param  $response
   */
  protected function _eventAuthReview($response)
  { 
    WC_ZipMoney::staticLog("WebHook Data:- ".json_encode($response), $this->log_enabled);
   
    try{    
        $order = new WC_Order($response->order_id);        
        
        if(!isset($order->id))
          throw new ZipMoney_Exception("Order with the given id not found");
       
        if($order->get_status() != "processing"){
          $order -> update_status('wc-zip-under-review','ZipMoney');
          WC_ZipMoney::staticLog(sprintf("ZipMoney Authorised Under Review [Order Id %s]. Status changed to Under Review", $response->order_id), $this->log_enabled);
        }
    } catch (Exception $e){
        WC_ZipMoney::staticLog($e->getMessage(),$this->log_enabled);
    }
    die();
  }
    
  /**
   * Process Authorisation Declined
   *
   * @access protected   
   * @throws ZipMoney_Exception
   * @param  $response
   */
  protected function _eventAuthDeclined($response)
  {   
    WC_ZipMoney::staticLog("WebHook Data:- ".json_encode($response), $this->log_enabled);

    try{
      $order = new WC_Order($response->order_id);
        
      if(!isset($order->id))
        throw new ZipMoney_Exception("Order with the given id not found");
       
      if($order->get_status() != "processing"){
        $order->update_status('wc-cancelled');
        WC_ZipMoney::staticLog(sprintf("ZipMoney Order Declined [Order Id %s]. Status changed to Cancelled. ", $response->order_id), $this->log_enabled);
      }
    } catch (Exception $e){
      WC_ZipMoney::staticLog($e->getMessage(),$this->log_enabled);
    }

    die();
  }

  /**
   * Process Cancel Success
   * 
   * @access protected   
   * @throws ZipMoney_Exception
   * @param  $response
   */
  protected function _eventCancelSuccess($response)
  {
    WC_ZipMoney::staticLog("WebHook Data:- ".json_encode($response), $this->log_enabled);

    try{
    	$order = new WC_Order($response->order_id);
        
      if(!isset($order->id))
        throw new ZipMoney_Exception("Order with the given id not found");
      
		  $order->update_status('wc-cancelled', 'ZipMoney');

      WC_ZipMoney::staticLog(sprintf("ZipMoney Cancelled [Order Id %s]. Status changed to wc-cancelled.", $response->order_id), $this->log_enabled);
    } catch (Exception $e){
      WC_ZipMoney::staticLog($e->getMessage(),$this->log_enabled);
    }

  	die();
  }

  /**
   * Process Cancel Fail
   *   
   * @access protected   
   * @throws ZipMoney_Exception
   * @param  $response
   */
  protected function _eventCancelFail($response)
  {   
    WC_ZipMoney::staticLog("WebHook Data:- ".json_encode($response), $this->log_enabled);

    try{
    	$order = new WC_Order($response->order_id);
            
      if(!isset($order->id))
        throw new ZipMoney_Exception("Order with the given id not found");

    		$order->add_order_note(sprintf('ZipMoney Cancel Failed %s ',$response->error_message));

        WC_ZipMoney::staticLog(sprintf("ZipMoney Cancel Failed [Order Id %s].",$response->order_id), $this->log_enabled);
      } catch (Exception $e){
        WC_ZipMoney::staticLog($e->getMessage(),$this->log_enabled);
      }
		die();
  }

  /**
   * Process Capture Success
   * 
   * @access protected   
   * @throws ZipMoney_Exception
   * @param  $response
   */
  protected function _eventCaptureSuccess($response)
  {  

    WC_ZipMoney::staticLog($this->getTxnId($response->order_id));
    
    try {
    	$order = new WC_Order($response->order_id);
            
      if(!isset($order->id))
          throw new ZipMoney_Exception("Order with the given id not found");
      
      if(
          $order->get_status() == "cancelled"     ||
          $order->get_status() == "refunded"    
        )
        throw new ZipMoney_Exception("The order has been refunded or cancelled");

      $order -> payment_complete($response->txn_id);
      $order -> update_status('wc-processing');

  		// create invoice
  		// send order email
      //$email = new WC_Email_Customer_Invoice();
      // $email->trigger($response->order_id);
      WC_ZipMoney::staticLog(json_encode($response), $this->log_enabled);
      WC_ZipMoney::staticLog(sprintf("ZipMoney Capture Success [Order Id %s]. Status changed to processing. ",$response->order_id), $this->log_enabled);
    } catch (Exception $e){
      WC_ZipMoney::staticLog($e->getMessage(),$this->log_enabled);
    }
    die();
  }

  /**
   * Process Capture Failure
   *
   * @access protected   
   * @throws ZipMoney_Exception
   * @param  $response
   */
  protected function _eventCaptureFail($response)
  {   
    WC_ZipMoney::staticLog("WebHook Data:- ".json_encode($response), $this->log_enabled);

    try{
    	$order = new WC_Order($response->order_id);
        
      if(!isset($order->id))
        throw new ZipMoney_Exception("Order with the given id not found");

      $order->update_status('wc-failed', 'ZipMoney');

     	$order->add_order_note(sprintf('ZipMoney Capture Failed %s ', $response->error_message));

      WC_ZipMoney::staticLog(sprintf("ZipMoney Capture Fail [Order Id %s] Error:- %s. Status changed to wc-zip-authorised.",$response->order_id,$response->error_message), $this->log_enabled);
    } catch (Exception $e){
      WC_ZipMoney::staticLog($e->getMessage(),$this->log_enabled);
    }

  	die();
  }

  /**
   * Process Refund Success
   *
   * @access protected   
   * @throws ZipMoney_Exception
   * @param  $response
   */
  protected function _eventRefundSuccess($response)
  {
    // Process Refund
    WC_ZipMoney::staticLog("WebHook Data:- ".json_encode($response), $this->log_enabled);

    $reference = get_post_meta($response->order_id,'_zipmoney_refund_reference',true);
    $order_id  = $response->order_id;
    
    try{
      if(!isset($order_id))
        throw new ZipMoney_Exception("Order id not found");

      $order   = wc_get_order( $order_id );
      
      $max_refund  = wc_format_decimal( $order->get_total() - $order->get_total_refunded() );
      $max_remaining_items = absint( $order->get_item_count() - $order->get_item_count_refunded() );

      if(!isset($order->id))
        throw new ZipMoney_Exception("Order with the given id not found");
     
      // If refund initiated by merchant
      if(!isset($response->reference) || $response->reference != $reference)
        $this->_refund($response);
      
      // Is full refund so update status to refunded
      if ( $response->refund_amount  == $max_refund && 0 === $max_remaining_items ) {       
        WC_ZipMoney::staticLog("Full Refund", $this->log_enabled);
        $order->update_status('wc-refunded');        
      } else {
        WC_ZipMoney::staticLog("Partial Refund", $this->log_enabled);
      }

    } catch (Exception $e){
      WC_ZipMoney::staticLog($e->getMessage(), $this->log_enabled);
    }

    die();
  }
  
  /**
   * Process Order Refund
   *
   * @access private   
   * @throws ZipMoney_Exception
   */
  private  function _refund($response)
  {

    $order_id               = $response->order_id;
    $refund_amount          = wc_format_decimal( $response->refund_amount );
    $refund_reason          = "";
    $restock_refunded_items = true;
    $refund                 = false;
    $response_data          = array();

    try {
      // Validate that the refund can occur
      $order       = wc_get_order( $order_id );
      $order_items = $order->get_items();
      $max_refund  = wc_format_decimal( $order->get_total() - $order->get_total_refunded() );

      if ( ! $refund_amount || $max_refund < $refund_amount || 0 > $refund_amount ) {
          throw new exception( __( 'Invalid refund amount', 'woocommerce' ) );
      }

      // Prepare line items which we are refunding
      $line_items = array();

      // Create the refund object
      $refund = wc_create_refund( array(
          'amount'     => $refund_amount,
          'reason'     => $refund_reason,
          'order_id'   => $order_id,
          'line_items' => $line_items
      ) );

      if ( is_wp_error( $refund ) ) {
          throw new ZipMoney_Exception( $refund->get_error_message() );
      }

      // Check if items are refunded fully
      $max_remaining_items = absint( $order->get_item_count() - $order->get_item_count_refunded() );

      if ( $refund_amount == $max_refund && 0 === $max_remaining_items ) {
          $order->update_status( apply_filters( 'woocommerce_order_fully_refunded_status', 'refunded', $order_id, $refund->id ) );
          $response_data['status'] = 'fully_refunded';
      }

      // Clear transients
      wc_delete_shop_order_transients( $order_id );
      
      WC_ZipMoney::staticLog(sprintf("ZipMoney Refund Success[Order Id %s]",$response->order_id), $this->log_enabled);

    } catch ( Exception $e ) {
      
      if ( $refund && is_a( $refund, 'WC_Order_Refund' ) ) 
          wp_delete_post( $refund->id, true );

      WC_ZipMoney::staticLog(sprintf("ZipMoney Refund Failed[Order Id %s]",$response->order_id), $this->log_enabled);
      WC_ZipMoney::staticLog($e->getMessage(), $this->log_enabled);
    }
  }

  /**
   * Process Refund Fail
   *  
   * @access protected   
   * @throws ZipMoney_Exception
   * @param  $response
   */
  protected function _eventRefundFail($response)
  {      
    WC_ZipMoney::staticLog("WebHook Data:- ".json_encode($response), $this->log_enabled);

    try{
    	$order = new WC_Order($response->order_id);

      if(!isset($order->id))
        throw new ZipMoney_Exception("Order with the given id not found");

      $order->add_order_note(sprintf('ZipMoney Refund Failed %s ', $response->error_message));

      WC_ZipMoney::staticLog(sprintf("ZipMoney Refund Fail [Order Id %s] Error:- %s",$response->order_id,$response->error_message), $this->log_enabled);
    } catch (Exception $e){
      WC_ZipMoney::staticLog($e->getMessage(),$this->log_enabled);
    }
  	die();
  }

  /**
   * Process Order Cancel
   *  
   * @access protected   
   * @throws ZipMoney_Exception
   * @param  $response
   */
  protected function _eventOrderCancel($response)
  {         
    WC_ZipMoney::staticLog("WebHook Data:- ".json_encode($response), $this->log_enabled);

    try{
    	$order = new WC_Order($response->order_id);
       
      if(!isset($order->id))
        throw new ZipMoney_Exception("Order with the given id not found");

		  if($order->get_status() != "processing"){
			 $order->update_status('wc-cancelled');
        WC_ZipMoney::staticLog(sprintf("ZipMoney Order Cancel [Order Id %s]. Status changed to Cancelled. ", $response->order_id), $this->log_enabled);
		  }
    } catch (Exception $e){
      WC_ZipMoney::staticLog($e->getMessage(),$this->log_enabled);
    }

		die();
	}

  /**
   * Process Charge Success
   *  
   * @access protected   
   * @throws ZipMoney_Exception
   * @param  $response
   */
  protected function _eventChargeSuccess($response)
  {         
    WC_ZipMoney::staticLog("WebHook Data:- ".json_encode($response), $this->log_enabled);

    try{ 

      $order = new WC_Order($response->order_id);
      
      if(!isset($order->id))
          throw new ZipMoney_Exception("Order with the given id not found");
      if(
          $order->get_status() == "cancelled"     ||
          $order->get_status() == "refunded"    
        )
        throw new ZipMoney_Exception("The order has been refunded or cancelled");

      $order -> payment_complete($response->txn_id);

      WC_ZipMoney::staticLog($this->getTxnId($response->order_id));
      // create invoice
      // send order email
      $order->update_status('wc-processing');

      WC_ZipMoney::staticLog(sprintf("ZipMoney Capture Success [Order Id %s]. ",$response->order_id), $this->log_enabled);
  
    } catch (Exception $e){
      WC_ZipMoney::staticLog($e->getMessage(),$this->log_enabled);
    }
	die();
	}

  /**
   * Process Charge Fail
   *
   * @access protected   
   * @throws ZipMoney_Exception
   * @param  $response
   */
  protected function _eventChargeFail($response)
  {   
      WC_ZipMoney::staticLog("WebHook Data:- ".json_encode($response), $this->log_enabled);

      try{

          $order = new WC_Order($response->order_id);            

          if(!isset($order->id))
              throw new ZipMoney_Exception("Order with the given id not found");

          $order->add_order_note(sprintf('ZipMoney Charge Failed %s ', $response->error_message));

          WC_ZipMoney::staticLog(sprintf("ZipMoney Charge Fail [Order Id %s] Error:- %s",$response->order_id,$response->error_message), $this->log_enabled);
      } catch (Exception $e){
          WC_ZipMoney::staticLog($e->getMessage(),$this->log_enabled);
      }
      die();
  }

  /**
   * Process Charge Fail
   *
   * @access protected   
   * @throws ZipMoney_Exception
   * @param  $response
   */
  protected function _eventConfigUpdate($response)
  {      
    WC_ZipMoney::staticLog("ZipMoney Config Update Initiated",$this->log_enabled);

    try{

      $gateway = WC_ZipMoney_Payment::instance();
      $wc_zipmoneyObj = new WC_ZipMoney($gateway);
      $wc_zipmoneyObj->handleConfigUpdate();

    } catch (Exception $e){
        WC_ZipMoney::staticLog($e->getMessage(),$this->log_enabled);
    }
    
    die();
  }

}
