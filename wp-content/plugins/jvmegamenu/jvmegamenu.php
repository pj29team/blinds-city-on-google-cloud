<?php

/*
 * Plugin Name: Hook event of Max Mega Menu
 * Plugin URI:  https://www.joomlavi.com
 * Description: Hook event megamenu_nav_menu_css_class of Max Mega Menu
 * Version:     1.0
 * Author:      Joomlavi
 * Author URI:  https://www.joomlavi.com
 */     
 
 add_filter( 'megamenu_nav_menu_link_attributes', 'jvnav_menu_link_attributes' , 1000, 3);
 
 function jvnav_menu_link_attributes( $atts, $item, $args ){
    
    if( !is_array( $atts ) ) {
        return array();
    }
    
    $uclasses = get_post_meta( $item->ID, '_menu_item_classes', true ); 
    
    if( !$uclasses ) { return $atts; }
    
    $uclasses = array_filter( $uclasses );       
    $uclasses = implode( " ", $uclasses );
    
    if( isset( $atts[ 'class' ] ) ) {
        
        $atts[ 'class' ] .= " {$uclasses}";
        
        return $attrs;        
    }
    
    $atts[ 'class' ] = $uclasses;
    
         
    return $atts;
        
 }