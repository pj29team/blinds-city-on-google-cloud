<?php

get_header();

global $product;

$_product = $product;

?>
<!--New banner-->
<?php /* <div id="new-banner-container" class="clearfix">
	<a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/new/banner_new.jpg" alt=""/></a>
</div> */ ?>
<?php echo do_shortcode( '[rev_slider new-big-banner]' ); ?>
<!--/New banner-->
<?php

$signup_form_show     = get_option('ocmx_signup_form');
$signup_form_image    = get_option('ocmx_signup_form_image');
$signup_form_title    = get_option('ocmx_signup_form_title');
$signup_form_subtitle = get_option('ocmx_signup_form_subtitle');

if (!$signup_form_show || ($signup_form_show === 'yes')): ?>
<!--Blinds signup form-->
<div id="blinds-signup-container">
	<div id="blinds-signup"<?php if ($signup_form_image): ?> style="background-image: url('<?php echo esc_attr($signup_form_image); ?>');"<?php endif; ?>>
		<form action="//blindscity.us13.list-manage.com/subscribe/post?u=3760e725864a061771776da4a&amp;id=c891e690be" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
			<div class="panel">
				<?php if ($signup_form_title): ?>
				<h1 class="panel-title"><?php echo $signup_form_title; ?></h1>
				<?php endif; ?>
				<?php if ($signup_form_subtitle): ?>
				<h2 class="panel-subtitle"><?php echo $signup_form_subtitle; ?></h2>
				<?php endif; ?>
				<div class="panel-columns">
					<div class="column-firstname">
						<!--<input type="text" name="cm-name" value=""/>-->
						<input type="text" value="" name="FNAME" class="" id="mce-FNAME">
						<label>Name</label>
					</div>
					<div class="column-email">
						<!--<input type="email" name="cm-kuxjl-kuxjl" value=""/>-->
						<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
						<label>Email Address</label>
					</div>
					 <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_3760e725864a061771776da4a_81eb065609" tabindex="-1" value=""></div>
					<div class="column-submit">
						<input type="submit" value="Join" name="subscribe" id="mc-embedded-subscribe" class="button">
						<!--<input type="submit" value="Join"/>-->
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</form>
	</div>
</div>
<!--/Blinds signup form-->
<?php endif; ?>
<div class="double-cloumn clearfix">
	<div class="full-width <?php echo ( is_front_page() ? 'homepage-wrapper':''); ?>">
		<?php do_action('woocommerce_before_single_product', $post, $_product); ?>
		<div class="products">
			<?php if ( have_posts() ) : ?>
			<?php
			
			do_action( 'woocommerce_before_shop_loop' ); 
			
			woocommerce_product_loop_start();
			woocommerce_product_subcategories(); 
			
			while ( have_posts() ) : the_post(); ?>
				<?php if( is_front_page() ){

						/*if( !has_term('dont-show-on-homepage','product_cat',$post) ){*/
							woocommerce_get_template_part( 'content', 'product' ); 
						/*}*/
						
					}else{
						woocommerce_get_template_part( 'content', 'product' ); 
					}
			endwhile; // end of the loop.
			
			?>
			<?php woocommerce_product_loop_end(); ?>
			<?php do_action( 'woocommerce_after_shop_loop' );?>
		<?php
		
		elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : 
			woocommerce_get_template( 'loop/no-products-found.php' ); 
		endif;
		
		?>
		</div>
		<?php motionpic_pagination("clearfix", "pagination clearfix"); ?>
	</div>
</div>


<?php

	if ( is_front_page() ){


		echo '<div id="home-content-wrapper"><div id="home-content" class="clearfix">';

		$the_query = new WP_Query( array(
        'page_id' => 10,
    	) );


		
		if ( $the_query->have_posts() ) {
			while ( $the_query->have_posts() )   {
			
			 $the_query->the_post();

			 the_content(); 
			
			} 
		} 


		echo '</div></div>';

		

	}
 
?>


<?php if ($green_delivery_text = get_option('ocmx_green_delivery_text')): ?>
<div id="free-delivery-green">
	<img src="<?php bloginfo('template_directory'); ?>/images/new/delivery-icon.png" alt=""/>
	<?php echo $green_delivery_text; ?>
</div>
<?php endif; ?>


<?php if ( is_active_sidebar( 'home-free-samples' ) ) : ?>
		<div class="free-samples-container">
			<?php dynamic_sidebar( 'home-free-samples' ); ?>
		</div>
<?php endif; ?>


<?php if ( is_active_sidebar( 'home-hi-pages' ) ) : ?>
		<div class="home-hipages-container">
			<?php dynamic_sidebar( 'home-hi-pages' ); ?>
		</div>
<?php endif; ?>

<div class="badge-icon-block">
	<h3>Why shop with blinds city?</h3>
	<img src="<?php bloginfo('template_directory'); ?>/images/badge-icons.png">
</div>
<?php get_footer(); ?>