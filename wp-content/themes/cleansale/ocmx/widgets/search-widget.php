<?php
class searchbox_widget extends WP_Widget {	
	function __construct() {
			$widget_ops = array( 'classname' => 'search-form', 'description' => 'Display a styled search field in your sidebar or footer.' );
			parent::__construct( 'searchbox_widget', '(Obox) Search', $widget_ops );
    }
     /** @see WP_Widget::widget */
	function widget($args, $instance) {
		// Turn $instance array into variables
		$instance_args = wp_parse_args( $instance);
		extract( $instance_args, EXTR_SKIP );
?>
	<li class="widget search-form column">
		<h4 class="widgettitle">
			<?php echo $instance['title']; ?>
        </h4>
        <form action="<?php echo home_url(); ?>" method="get" class="search-form">
            <input type="text" name="s" id="s" class="search" maxlength="50" value="<?php the_search_query(); ?>" placeholder="Search" />
            <input type="submit" class="search_button" value="<?php _e("Search", "ocmx"); ?>" />
        </form>
	</li>
<?php
    }

     /** @see WP_Widget::update */
	function update($new_instance, $old_instance) {
		return $new_instance;
	}

	/** @see WP_Widget::form */
	function form($instance) {
		// Turn $instance array into variables
		$instance_defaults = array ( 'title' => 'Search');
		$instance_args = wp_parse_args( $instance, $instance_defaults );
		extract( $instance_args, EXTR_SKIP ); ?>
   		    		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title', 'ocmx'); ?><input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></label></p>	
	<?php } // form

}// class

//This sample widget can then be registered in the widgets_init hook:

// register FooWidget widget
add_action('widgets_init', create_function('', 'return register_widget("searchbox_widget");'));

?>