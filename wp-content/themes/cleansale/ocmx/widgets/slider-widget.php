<?php
class slider_widget extends WP_Widget {
	/** constructor */
	function __construct() {
			$widget_ops = array( 'classname' => 'slider_widget', 'description' => 'Home Page Widget - Display featured posts or products a tabbed slider.' );
			parent::__construct( 'slider_widget', '(Obox) Slider', $widget_ops );
    }
	/** @see WP_Widget::widget */
	function widget($args, $instance) {
		// Turn $args array into variables.
		extract( $args );

		// Turn $instance array into variables
		$instance_defaults = array('title' => '', 'post_thumb' => 1, 'display_limit' => '3', 'auto_interval' => '0');
		$instance_args = wp_parse_args( $instance, $instance_defaults );
		extract( $instance_args, EXTR_SKIP );

		if(isset($instance["postfilter"]))
			$postfilter = $instance["postfilter"];
		$filterval = $instance[$postfilter];
		if(isset($instance["post_category"]))
			$use_category = $instance["post_category"];


		if(isset($instance["post_thumb"]))
			$post_thumb = $instance["post_thumb"];

		if(isset($post_thumb) && $post_thumb == "true") :
			$post_thumb = 1;
		else :
			$post_thumb = 0;
		endif;

		if(isset($postfilter) && $postfilter != "" && $filterval != "0") :
			$args = array(
				"post_type" => $posttype,
				"posts_per_page" => $display_limit,
				"tax_query" => array(
					array(
						"taxonomy" => $postfilter,
						"field" => "slug",
						"terms" => $filterval
					)
				)
			);
		else :
			$args = array(
				"post_type" => $posttype,
				"posts_per_page" => $display_limit,
			);
		endif;

		// Set the post order
		if(isset($post_order_by)) :
			$args['order'] = $post_order;
			$args['orderby'] = $post_order_by;
		endif;

		$ocmx_featured = new WP_Query($args);
?>
	<div class="feature clearfix">
		<ul class="left-column">
			<?php $count=1;
			while ($ocmx_featured->have_posts()) : $ocmx_featured->the_post();
				global $post, $product;
				$_product = $product;
				$args  = array('postid' => $post->ID, 'width' => 630, 'height' => 380, 'hide_href' => true, 'exclude_video' => $post_thumb, 'imglink' => false, 'imgnocontainer' => true, 'resizer' => '630x380');
				$image = get_obox_media($args);
				$default_link = get_permalink($post->ID);
				$custom_link = get_post_meta($post->ID, "sliderlink", true);
				if($custom_link !="") :
					$link = $custom_link;
				else :
					$link = $default_link;
				endif; ?>

				<li class="slider-image <?php if($count !== 1): ?>no_display<?php else : ?>active<?php endif; ?>" id="feature-copy-<?php echo $count; ?>">
					<div class="post">
						<div class="post-image">
							<a href="<?php echo $link; ?>">
								<?php echo $image; ?>
							</a>
						</div>
						<div class="overlay" <?php if($count !== 1): ?>style="display: none;"<?php endif; ?>>
							<h2 class="post-title"><a href="<?php echo $link; ?>"><?php the_title(); ?><?php if($posttype == 'product'): ?> <span class="price"><?php echo $_product->get_price_html(); ?></span><?php endif; ?></a></h2>
							<?php the_excerpt(); ?>
						</div>
					</div>
				</li>
			<?php
				$count++;
			endwhile; ?>
			<a class="next" href="#"></a>
			<a class="previous" href="#"></a>

			<div id="slider-auto-<?php echo $use_category; ?>" class="no_display"><?php echo $auto_interval; ?></div>
		</ul>
		<div class="right-column">
			<div class="container">
				<ul class="column">
					<?php  $count=1;
					while ($ocmx_featured->have_posts()) : $ocmx_featured->the_post();
						global $post, $product;
						$_product = $product;
						$args  = array('postid' => $post->ID, 'width' => 250, 'height' => 160, 'hide_href' => true, 'exclude_video' => true, 'imglink' => false, 'imgnocontainer' => true, 'resizer' => '250x165');
						$image = get_obox_media($args); ?>

						<li <?php if($count == 1): ?>class="active"<?php endif; ?> id="feature-li-<?php echo $count; ?>">
							<a class="post-image" href="#" id="feature-href-<?php echo $count; ?>">
								<?php echo $image; ?>
							</a>
							<div class="overlay">
								<h2 class="post-title">
									<a href="#" id="feature-href-<?php echo $count; ?>"><?php the_title(); ?><?php if($posttype == 'product'): ?> <span class="price"><?php echo $_product->get_price_html(); ?></span><?php endif; ?></a>
								</h2>
							</div>
						</li>
					<?php
						$count++;
					endwhile; ?>
				</ul>
			</div>
			<a class="previous" href="#"></a>
			<ul class="slider-dots">
				<?php for($i = 1; $i <= floor($count/2); $i++) : ?>
					<li <?php if($i == 1) : ?>class="active"<?php endif; ?>><a href="#" style="display: block;"><?php echo $i; ?></a></li>
				<?php endfor; ?>
			</ul>
			<a class="next" href="#"></a>
		</div>
	</div>
<?php
	}

	/** @see WP_Widget::update */
	function update($new_instance, $old_instance) {
		return $new_instance;
	}

	/** @see WP_Widget::form */
	function form($instance) {
			// Turn $instance array into variables
		$instance_defaults = array('title' => '', 'post_thumb' => 1, 'display_limit' => '3', 'auto_interval' => '0');
		$instance_args = wp_parse_args( $instance, $instance_defaults );
		extract( $instance_args, EXTR_SKIP );


		// Setup the post filter if it's defined
		if(isset($postfilter) && isset($instance[$postfilter]))
			$filterval = esc_attr($instance[$postfilter]);
		else
			$filterval = 0;

		$post_type_args = array("public" => true, "exclude_from_search" => false, "show_ui" => true);
		$post_types = get_post_types( $post_type_args, "objects");
?>
		<p><em>Each time you select an item from a dropdown, press "Save" to load the next set of settings.</em></p>
		<p>
			<label for="<?php echo $this->get_field_id('posttype'); ?>">Display</label>
			<select size="1" class="widefat" id="<?php echo $this->get_field_id("posttype"); ?>" name="<?php echo $this->get_field_name("posttype"); ?>">
				<option <?php if(isset($posttype) && $posttype == ""){echo "selected=\"selected\"";} ?> value="">--- Select a Content Type ---</option>
				<?php foreach($post_types as $post_type => $details) : ?>
					<option <?php if(isset($posttype) && $posttype == $post_type){echo "selected=\"selected\"";} ?> value="<?php echo $post_type; ?>"><?php echo $details->labels->name; ?></option>
				<?php endforeach; ?>
			</select>
		</p>

		<?php if(isset($posttype) && $posttype != "") :
			if($posttype != "page") : ?>
				<?php $taxonomyargs = array('post_type' => $posttype, "public" => true, "exclude_from_search" => false, "show_ui" => true);
				$taxonomies = get_object_taxonomies($taxonomyargs,'objects');
				if(!empty($taxonomies)) : ?>
					<p>
						<label for="<?php echo $this->get_field_id('postfilter'); ?>">Filter by</label>
						<select size="1" class="widefat" id="<?php echo $this->get_field_id("postfilter"); ?>" name="<?php echo $this->get_field_name("postfilter"); ?>">
							<option <?php if(isset($postfilter) && $postfilter == ""){echo "selected=\"selected\"";} ?> value="">--- Select a Filter ---</option>
							<?php foreach($taxonomies as $taxonomy => $details) : ?>
								<option <?php if(isset($postfilter) && $postfilter == $taxonomy){echo "selected=\"selected\"";} ?> value="<?php echo $taxonomy; ?>"><?php echo $details->labels->name; ?></option>
							<?php $validtaxes[] = $taxonomy;
							endforeach; ?>
						</select>
					</p>
				<?php endif;
				if(isset($postfilter) != "" && ( (is_array(isset($validtaxes)) && in_array($postfilter, $validtaxes)) || !is_array(isset($validtaxes)) ) ) :
					$tax = get_taxonomy($postfilter);
					$terms = get_terms($postfilter, "orderby=count&hide_empty=0"); ?>
					<p><label for="<?php echo $this->get_field_id($postfilter); ?>"><?php if(isset($tax->labels->name)) echo $tax->labels->name; ?></label>
					   <select size="1" class="widefat" id="<?php echo $this->get_field_id($postfilter); ?>" name="<?php echo $this->get_field_name($postfilter); ?>">
							<option <?php if(isset($filterval) == 0){echo "selected=\"selected\"";} ?> value="0">All</option>
							<?php foreach($terms as $term => $details) :?>
								<option  <?php if(isset($filterval) && $filterval == $details->slug){echo "selected=\"selected\"";} ?> value="<?php echo $details->slug; ?>"><?php echo $details->name; ?></option>
							<?php endforeach;?>
						</select>
					</p>
				<?php endif; ?>

			<?php endif; ?>
		  <?php endif; ?>

		<p>
			<label for="<?php echo $this->get_field_id('post_thumb'); ?>">Thumbnails</label>
			<select size="1" class="widefat" id="<?php echo $this->get_field_id('post_thumb'); ?>" name="<?php echo $this->get_field_name('post_thumb'); ?>">
					<option <?php if(isset($post_thumb) && $post_thumb == "true") : ?>selected="selected"<?php endif; ?> value="true">Post Feature Image</option>
					<option <?php if(isset($post_thumb) && $post_thumb == "false") : ?>selected="selected"<?php endif; ?> value="false">Videos</option>
			</select>
		 </p>
		 <p><label for="<?php echo $this->get_field_id('display_limit'); ?>">Post Count</label>
			<select size="1" class="widefat" id="<?php echo $this->get_field_id('display_limit'); ?>" name="<?php echo $this->get_field_name('display_limit'); ?>">
				<?php for($i = 1; $i < 21; $i++) : ?>
					<option <?php if(isset($display_limit) && $display_limit == $i) : ?>selected="selected"<?php endif; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
				<?php endfor; ?>
			</select>
		</p>

		<?php  // Setup the order values
		$order_params = array("date" => "Post Date", "title" => "Post Title", "rand" => "Random",  "comment_count" => "Comment Count",  "menu_order" => "Menu Order"); ?>
		<p>
			<label for="<?php echo $this->get_field_id('post_order_by'); ?>"><?php _e("Order By", "ocmx"); ?></label>
			<select size="1" class="widefat" id="<?php echo $this->get_field_id('post_order_by'); ?>" name="<?php echo $this->get_field_name('post_order_by'); ?>">
				<?php foreach($order_params as $value => $label) :?>
					<option  <?php if(isset($post_order_by) && $post_order_by == $value){echo "selected=\"selected\"";} ?> value="<?php echo $value; ?>"><?php echo $label; ?></option>
				<?php endforeach;?>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('post_order'); ?>"><?php _e("Order", "ocmx"); ?></label>
			<select size="1" class="widefat" id="<?php echo $this->get_field_id('post_order'); ?>" name="<?php echo $this->get_field_name('post_order'); ?>">
				<option <?php if(!isset($post_order) || isset($post_order) && $post_order == "DESC") : ?>selected="selected"<?php endif; ?> value="DESC"><?php _e("Descending", 'ocmx'); ?></option>
				<option <?php if(isset($post_order) && $post_order == "ASC") : ?>selected="selected"<?php endif; ?> value="ASC"><?php _e("Ascending", 'ocmx'); ?></option>
			</select>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('auto_interval'); ?>">Auto Slide Interval (seconds)
				<input class="shortfat" id="<?php echo $this->get_field_id('auto_interval'); ?>" name="<?php echo $this->get_field_name('auto_interval'); ?>" type="text" value="<?php if(isset($auto_interval)) echo $auto_interval; ?>" /><br /><em>(Set to 0 for no auto-sliding)</em>
			</label>
		</p>
<?php
	} // form

}// class

//This sample widget can then be registered in the widgets_init hook:

// register FooWidget widget
add_action('widgets_init', create_function('', 'return register_widget("slider_widget");'));

?>