<?php
class ocmx_comment_widget extends WP_Widget {
    /** constructor */
	function __construct() {
			$widget_ops = array( 'classname' => 'widget_recent_comments', 'description' => 'Display a feed of recent comments on posts.' );
			parent::__construct( 'ocmx_comment_widget', '(Obox) Comments', $widget_ops );
    }
    /** @see WP_Widget::widget */
	function widget($args, $instance) {
		// Turn $instance array into variables
		$instance_defaults = array ('comment_count' => 5);
		$instance_args = wp_parse_args( $instance, $instance_defaults );
		extract( $instance_args, EXTR_SKIP );
		
		global $wpdb;
		
		$latest_comments = $wpdb->get_results($wpdb->prepare( "SELECT * FROM $wpdb->comments WHERE comment_approved = 1 ORDER BY comment_date DESC LIMIT ".$comment_count, "ARRAY_A") );
		
		?>
        <li id="%1$s" class="widget %2$s">
          <div class="content">
            <h4 class="widgettitle"> 
            	<?php echo $instance['title']; ?>
            </h4>
                <ul>
                    <?php foreach($latest_comments as $latest_comment) : 
                        $this_comment = get_comment($latest_comment->comment_ID);
                        $use_id = $this_comment->comment_post_ID;
                        $this_post = get_post($use_id); 
                        $post_title = $this_post->post_title;
                        $post_link = get_permalink($this_comment->comment_post_ID);
                    ?>
                    <li>
                        <?php $use_comment = apply_filters('wp_texturize', $this_comment->comment_content);
                        $comment_length = strlen(strip_tags($use_comment));
                        $use_comment = strip_tags(substr($use_comment, 0 , 100));
                        if($comment_length > 100) : $use_comment .= "..."; endif;
                        echo $use_comment; ?>
                        <a href="<?php echo $post_link; ?>#comment-<?php echo $this_comment->comment_ID; ?>"><?php echo $this_comment->comment_author; ?> <span>in</span> <?php echo $post_title; ?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
          </div>
       </li>
<?php
    }

  /** @see WP_Widget::update */
	function update($new_instance, $old_instance) {
		return $new_instance;
	}

	/** @see WP_Widget::form */
	function form($instance) {
		// Turn $instance array into variables
		$instance_defaults = array ( 'title' => 'Latest Comments');
		$instance_args = wp_parse_args( $instance, $instance_defaults );
		extract( $instance_args, EXTR_SKIP );
		
        ?>
         <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title', 'ocmx'); ?><input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></label></p>
            <p>
            	<label for="<?php echo $this->get_field_id('comment_count'); ?>"><?php _e('Comment Count', 'ocmx'); ?></label>
                <select size="1" class="widefat" id="<?php echo $this->get_field_id('comment_count'); ?>" name="<?php echo $this->get_field_name('comment_count'); ?>">
                	<?php for($i = 1; $i < 10; $i++) : ?>
	                    <option <?php if($comment_count == $i) : ?>selected="selected"<?php endif; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                    <?php endfor; ?>
                </select>
			</p>
<?php 
	} // form

}// class

//This sample widget can then be registered in the widgets_init hook:

// register FooWidget widget
add_action('widgets_init', create_function('', 'return register_widget("ocmx_comment_widget");'));

?>
